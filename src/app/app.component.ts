import {Component, OnInit} from '@angular/core';
import {RustService} from "./services/rust.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  recievedData: any[];
  nbViews = '';

  public constructor(private rustService: RustService) {
    this.recievedData = [];
  }

  public ngOnInit() {
    // this.rustService.getRustElements(1).subscribe((data) => {
    //   console.log(data[0])
    //   this.recievedData = data;
    //
    //   this.rustService.getNbViews().subscribe((nbViews) => {
    //     this.nbViews = nbViews;
    //   })
    // });
  }

  public getUserData(user_id: number) {
    this.rustService.getRustElements(user_id).subscribe((data) => {
      console.log(data[0])
      this.recievedData = data;

      this.rustService.getNbViews().subscribe((nbViews) => {
        this.nbViews = nbViews;
      })
    });
  }
}
