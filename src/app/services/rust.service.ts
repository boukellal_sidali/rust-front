import { Injectable } from '@angular/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RustService {

  constructor(private http: HttpClient) { }

  public getRustElements(user_id: number): Observable<any[]>{
    return this.http.get<any[]>('http://127.0.0.1:8000/getData/'+ user_id);
  }

  public getNbViews(): Observable<string>{
    return this.http.get<string>('http://127.0.0.1:8000/getNbViews');
  }

}
